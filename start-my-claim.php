
	<?php  include 'includes/header.php';?>

	<div class="intro" style="background-image: url('assets/images/Non-Fault-Accident-Claim-1.jpg');">
		<div class="container">
			<div class="row">
				<div class="col-xl-8">
					<div class="squarebcon1">
						<h1 class="boldwhite p5vh">Make a <span class="gradient-45">Non Fault Claim</span> After A Non Fault Accident.</h1>
						<p class="boldwhite">Have you just been involved in a road traffic accident that wasn’t your fault? We can help you from where you are now, through to getting you back on the road
						All at no cost to you.</p>
						<p class="boldwhite">Start your claim below</p>
					</div>
					<div class="squarebcon1">
						<p class="boldwhite animate delaylowfast animated" style="font-size:30px">More often than not, car accidents happen quickly and unexpectedly. It's important to take certain steps in order for you to make a successful Non Fault Claim.</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="request_form-block">
		<div class="container">
			<h2 class="p5vh">Make a <span class="gradient-45">Non Fault</span> Claim</h2>
			<p class="pheaders p3vh">After A Non Fault Accident.</p>
			<div class="form-wrap mt-5">
				<p><span style="font-size: 30px;">Start a <span class="gradient-pu">non-fault claim</span> using our claims form here:</span><br> <span class="regdark grdar" style="font-size: 16px;">Our <b>7 step claim form</b> will take information regarding your non-fault accident for one of our <b>New Claims handlers</b> to assess. They will get in touch with you to discuss your claim and the next step. You can also call us now on <a href="tel:+443301654373">0330 165 4373</a>.</span></p>
				<form method="post" action="">
					<div class="page" id="page1">
						<p>Have you been involved in a road traffic accident?</p>
						<label>
							<input type="radio" name="page1_option" value="Option 1" checked> Yes, I've had an accident
						</label>
						<br>
						<label>
							<input type="radio" name="page1_option" value="Option 2"> No, I haven't had an accident
						</label>
						<br>
						<br>
						<button type="button" class="btn btn-primary" onclick="nextPage(1)">Next</button>
					</div>

					<div class="page" id="page2" style="display: none;">
						<p>Was the accident your fault?</p>
						<label>
							<input type="radio" name="page2_option" value="Option 1" checked> Option 1
						</label>
						<br>
						<label>
							<input type="radio" name="page2_option" value="Option 2"> Option 2
						</label>
						<br>
						<br>
						<button type="button" class="btn btn-primary" onclick="prevPage(2)">Previous</button>
						<button type="button" class="btn btn-primary" onclick="nextPage(2)">Next</button>
					</div>
					<div class="page" id="page3" style="display: none;">
						<p>Is your vehicle drivable?</p>
						<label>
							<input type="radio" name="page2_option" value="Option 1" checked> Option 1
						</label>
						<br>
						<label>
							<input type="radio" name="page2_option" value="Option 2"> Option 2
						</label>
						<br>
						<br>
						<button type="button" class="btn btn-primary" onclick="prevPage(3)">Previous</button>
						<button type="button" class="btn btn-primary" onclick="nextPage(3)">Next</button>
					</div>

					<div class="page" id="page4" style="display: none;">
						<p>Were you physically injured in the accident?</p>
						<label>
							<input type="radio" name="page2_option" value="Option 1" checked> Option 1
						</label>
						<br>
						<label>
							<input type="radio" name="page2_option" value="Option 2"> Option 2
						</label>
						<br>
						<br>
						<button type="button" class="btn btn-primary" onclick="prevPage(4)">Previous</button>
						<button type="button" class="btn btn-primary" onclick="nextPage(4)">Next</button>
					</div>

					<div class="page" id="page5" style="display: none;">
						<p>Have you contacted your own insurer or any other organisation about this accident already?</p>
						<label>
							<input type="radio" name="page3_option" value="Option 1" checked> Option 1
						</label>
						<br>
						<label>
							<input type="radio" name="page3_option" value="Option 2"> Option 2
						</label>
						<br>
						<br>
						<button type="button" class="btn btn-primary" onclick="prevPage(5)">Previous</button>
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
					
				</form>
			</div>
		</div>
	</div>

	<script>
        function showPage(pageNumber) {
            // Hide all pages
            var pages = document.getElementsByClassName("page");
            for (var i = 0; i < pages.length; i++) {
                pages[i].style.display = "none";
            }

            // Show the selected page
            document.getElementById("page" + pageNumber).style.display = "block";
        }

        function nextPage(currentPage) {
            showPage(currentPage + 1);
        }

        function prevPage(currentPage) {
            showPage(currentPage - 1);
        }
    </script>

	<div class="content-box lg">
		<div class="container">
		<p>Road Traffic Accident Claims are daunting, there’s no doubt about it. Our team are Non Fault Claim Experts. We’ll help you with:</p>
		<div class="row">
			<div class="col-xl-6 grdar">
				<ul class="custombullets">
					<li>Advice and guidance on <a href="#">what to do after a car accident</a>.</li>
					<li>Help to <a href="#">avoid you paying your insurance excess</a> fee.</li>
					<li>Provide <a href="#">recovery</a> of your vehicle at no cost to you.</li>
					<li>Arrange a like-for-like <a href="#">replacement vehicle</a> quickly.</li>
				</ul>
			</div>
			<div class="col-xl-6 grdar">
				<ul class="custombullets">
					<li>Manage your full <a href="#">accident claim</a>, start to finish.</li>
					<li>Help <a href="#">avoid an effect on your insurance</a> policy.</li>
					<li>Assist with any <a href="#">personal injury claims</a>.</li>
					<li>Have your vehicle repaired and get you back on the road.</li>
				</ul>
			</div>
		</div>
		</div>
	</div>

	<div class="blb" style="background-image: url('assets/images/Speaking-To-Our-Team-After-A-Non-Fault-Accident.jpg');background-position: center center;   background-repeat: no-repeat; background-size: cover;">
		<div class="container">
			<div class="row">
				<div class="col-xl-9">
					<div class="grwhi">
						<h3 class="boldwhite p4vh">Ready to speak to our team about <br>your <span class="gradient-45">non fault accident</span>?</h3>
						<p class="regwhite"><b>Do you have more questions about <a href="#">what a non fault claim is</a>? Fill in our non-fault accident claim form above</b> and one of our Non Fault Claim Experts will be in touch as soon as possible to begin the process and answer any questions that you may have at this point.</p>
						<p class="boldwhitett"> Alternatively, you can call us now on <a href="#">0330 165 4208</a>.</p>
					</div>
				</div>
			</div>
			<div class="blck-height" style="height: 800px;"></div>
			<div class="row">
				<div class="col-xl-3">
					<p class="boldwhite">Our head office is open:</p><p> <span class="regwhite">Mon-Fri | 8am-10pm<br> Sat | 8:30am-2:30pm<br> Sun | 10am-4pm</span></p>
				</div>
				<div class="col-xl-9">
					<div class="grwhi">
						<p class="boldwhite">Outside of these times: Our Out Of Hours Team is available to call 24/7 also on <a href="tel:+443301654208" class="boldwhite">0330 165 4208</a>.</p>
						<p class="boldwhite"> They can help with <a href="#">vehicle recovery</a> and collecting details about the accident, ready for one of our head office team to call you back.</p>
						<p class="boldwhitett">Processing a Claim with an <a href="#">Accident Management Company</a> is the most beneficial step to take if the accident wasn’t your fault.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xl-12">
					<p class="pheaders boldwhite p4vh">We take the stress <span class="gradient-45">out of an accident</span>.</p>
				</div>
			</div>
		</div>
	</div>

	<div class="block-1" style="background-image: url('assets/images/We-Take-The-Stress-Out-Of-An-Accident.jpg');background-position: center center;  background-repeat: no-repeat;  background-size: cover;">

	</div>

	<div class="blb block-2" style="background-image: url('assets/images/Starting-A-Non-Fault-Claim.jpg'); background-position: bottom center;
  background-repeat: no-repeat;">
		<div class="container">
			<div class="row">
				<div class="col-xl-8 offset-xl-2 mb-5">
					<h3 class="boldwhite text-center p5vh">What Happens <span class="gradient-pu">After You Begin</span> <br>A Non Fault Claim.</h3>
					<p class="regwhite">If you’re unfortunate enough to be involved in an accident that wasn’t your fault, a non fault claim is a specific road traffic accident claim process which allows you to claim for damages, losses and injuries.</p><p class="gradient-pu">The difference between standard road traffic accident claims and a non fault claim is:</p><ul class="custombullets regwhite"><li>How the process begins</li><li>The difference in entitled benefits that the non-fault drivers receive</li></ul>
				</div>
			</div>
			<div class="row">
				<div class="col-xl-6 offset-xl-6">
					<div class="puwhi">
						<p class="regwhite"><span class="gradient-pu">As the non-fault driver</span>, it’s not fair that you have been involved in this scenario. It wasn’t you who DIDN’T follow <a href="https://www.gov.uk/browse/driving/highway-code-road-safety" target="_blank" rel="noopener">the rules of the road</a>, yet it IS YOU who has been hit with a range of inconveniences as a result.</p><p class="boldwhitett">So, the accident market established <a href="/claims-management/">accident claims management</a> (also known as <a href="/accident-management-company/">Accident Management</a> and <a href="/credit-hire/">Credit Hire</a>).</p>
					</div>
				</div>
			</div>
			
		</div>
	</div>

	<div class="block-3 blb">
		<div class="container">
			<h3 class="boldwhite p4vh">What is Accident <span class="gradient-45">Claims Management</span>?</h3>
			<div class="row">
				<div class="col-xl-6">
					<p class="boldwhite">This is a process where the non-fault driver makes a non fault claim through the at-fault driver’s insurance policy directly.</p>
					<p class="regwhitett">This leaves out the requirement to make a claim against the non-fault driver’s own insurance policy.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-xl-6 offset-xl-6">
					<p class="boldwhitett">Then in the process, the non-fault driver receives a series of benefits to help reduce the impact on their day-to-day life – all at no cost to them. The costs are usually covered by a credit hire agreement, until they are reclaimed back from the at-fault driver’s insurer.</p>
				</div>
			</div>
		</div>
	</div>
	<div class="block-4" style="
		background-image: url('assets/images/How-The-Process-Begins.jpg');
		background-position: center center;
		background-repeat: no-repeat;
		background-size: cover;
		height: 1000px;
		">

	</div>

	<div class="block-5 blb">
		<div class="containeer">
			<div class="row">
				<div class="col-xl-8 offset-xl-2 text-center">
					<div class="squarebcon1 grwhi">
						<h3 class="boldwhite p4vh">How the <span class="gradient-45">process begins</span> for a non fault claim.</h3><p class="boldwhite">A Non Fault Accident Claim must begin by speaking to an <a href="/accident-management-company/">Accident Management Company</a> first.</p><p class="boldwhite">Many people have a knee-jerk reaction to instinctively contact their own insurance company first after an accident.</p><p class="regwhite">Unfortunately, if a claim initiates as a result of the non-fault driver contacting their insurer directly, the driver who wasn’t at fault is no longer making a specific No Fault Claim.</p><p class="regwhite">Instead, the first step is to contact Accident Management Specialists like ourselves at Auto Claims Assist.</p><p class="boldwhitett">From here, we take as much of the information that you currently have, open your accident claim and will also provide accident recovery if your vehicle is still stuck at the roadside.</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="black-bg better-alt blb">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="left-content">
						<div class="squarebcon1 ct-white-pur text-center">
							<h4 class="boldwhite">We are the <span class="gradient-pu">better alternative</span> than contacting your own insurer.</h4>
							<br>
							<p class="click-through boldwhite">Call our team now <a href="tel:+443309128010">0330 912 8010</a> | <a href="#"><span class="ct-under-pur">Start your claim now</span> <span class="ct-arrow-pur">&gt;</span></a></p>
						</div>
						<div class="squarebcon1 text-center">
							<p class="regwhite">From here, we take as much of the information that you currently have, open your accident claim and will also provide accident recovery if your vehicle is still stuck at the roadside.</p><p class="gradient-pu">
								<a href="/accident-management-company">Learn about Auto Claims Assist</a>
							</p>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="image">
						<img src="assets/images/Starting-A-Claim-Recovery.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="black-bg better-alt blb">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="left-content">
						<div class="squarebcon1 ct-white-pur text-center">
							<h4 class="boldwhite"><span class="gradient-pu">Preparing</span> your claim and providing you with a replacement.</h4><p class="regwhite">You’ll be assigned a dedicated claims handler who will communicate everything that we need as we prepare your claim.</p><p class="gradient-pu">This will include:</p>
						</div>
						<div class="squarebcon1">
							<ul class="custombullets regwhite">
								<li>Evidence from the scene (i.e. pictures, videos and documented notes)</li><li>Any witness and passenger reports</li>
								<li>Police reports, including medical and injury documents</li>
								<li>Plus more</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					
					<div class="image">
						<div class="squarebcon1 puwhi">
							<p class="regwhite">Knowing <a href="/road-traffic-accident-resource/what-to-do-after-a-car-accident/">what to do in a car accident</a> is very helpful in evidence collecting at the scene and understanding how to demonstrate who was at fault.</p><p class="regwhitett"> Once we understand the accident circumstances and are happy in being able to process a claim, we will organise a <a href="/vehicle-replacement/">replacement vehicle</a> for you while yours is off the road. The replacement vehicle is with you from start to finish, no matter how long your claim takes to process.</p>
						</div>
						<div class="img">
							<img src="assets/images/Providing-You-With-A-Replacement.jpg" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="black-bg better-alt blb">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="left-content">
						<div class="squarebcon1 ct-white-pur text-center">
						<h4 class="boldwhite"><span class="gradient-pu">Your Vehicle</span> Repair and Claiming for Injuries.</h4><p class="regwhite">Over our many years in <a href="/accident-management-company/">Accident Management</a>, we have established a repair network of accident repair centres and repair garages that are <a href="https://www.bsigroup.com/en-GB/kitemark/" target="_blank" rel="noopener">BSI Kitemark accredited</a>, providing the highest quality of repair.</p>
						</div>
						<div class="squarebcon1 puwhi">
							<p class="regwhite">Our entire repair network provides only manufacturer-approved parts and manufacturer-approved paints, ensuring your vehicle is repaired to its intended standard.</p> <a href="/accident-repair-centre/car-accident-repair/">Learn about Vehicle Repair</a>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					
					<div class="image">
						<div class="squarebcon1 puwhi">
							<p class="boldwhitett">If any injuries had occurred in the accident, we have a panel of expert personal injury solicitors who organise <a href="/personal-injury/">personal injury claims</a> for you and your passengers.</p>
						</div>
						<div class="img">
							<img src="assets/images/Vehicle-Repair.jpg" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="black-bg better-alt blb">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="left-content">
						<div class="squarebcon1 ct-white-pur text-center">
							<h4 class="boldwhite"><span class="gradient-pu">Getting</span> you back on the road.</h4><p class="regwhite">Leaving no stone unturned, we get you back onto the road in your own vehicle as quickly, safely and efficiently as possible.</p>
						</div>
						<div class="squarebcon1 puwhi">
							<p class="regwhite">If your vehicle lost value due to the accident it was involved in, we’ll also organise and manage the diminution claim. As non fault claim specialists, we are independent and on your side; here to provide a service which avoids any out-of-pocket or negative effects as a result of the accident.</p> <a href="/claims-management/">Learn about Claims Management</a>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="image">
						<div class="img">
							<img src="assets/images/Getting-You-Back-On-The-Road.jpg" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="block-6 lg">
		<div class="container">
			<div class="row">
				<div class="col-xl-6">
					<p class="pheaders p7vh">Your First <br>Contact After <span class="gradient-pu"><br>The Accident</span>.</p>
				</div>
			</div>
			
			<div class="row">
				<div class="col-xl-6 offset-xl-6 pudar">
					<p class="regbold">Your first contact after the accident, if it wasn’t your fault, should be an <a href="#">accident management company</a> (also known as a <a href="#">credit hire company</a>) like Auto Claims Assist.</p>
				</div>
			</div>
		</div>
	</div>

	<div class="block-7 lg">
		<div class="container" style="background-color: #ffffff;">
			<div class="row">
				<div class="col-xl-6">
					<div class="image">
						<img src="assets/images/Contacting-Auto-Claims-Assist-1.webp" alt="">
					</div>
				</div>
				<div class="col-xl-6">
					<div class="squarebcon1 text-center">
						<p>Some people think that the first thing they need to do after a non-fault accident is to contact their own insurance company.</p><p class="regdark pudar">In light of this, we have created a comparison between calling an <a href="/accident-management-company/accident-management-company-vs-insurance-company/">accident management company vs your insurance company here</a> after a non-fault accident.</p><p class="regdark">Using your own insurer could leave you worse off if the accident wasn’t your fault.</p> <br>
						<div style="text-align: center;"><a href="/non-fault-claim" button="" class="claimcta">Start my claim</a></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="claim-block">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 mb-5">
				<p class="pheaders p5vh">By contacting <span class="gradient-45">Auto Claims Assist</span> first, you'll be entitled to a <span class="gradient-pu">much better</span> experience and at no cost to you:</p>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 col-md-4 col-xl-3 mb-3">
					<div class="item mb-3">
						<img class="mb-3" src="assets/images/Rated-Excellent-on-Trustpilot_3.svg" alt="">
						<p><b>Rated</b> EXCELLENT on Trustpilot</p>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 col-xl-3 mb-3">
					<div class="item mb-3">
						<img class="mb-3" src="assets/images/Award-Winning_2.svg" alt="">
						<p><b>Award-Winning</b> and FCA Regulated.</p>
					</div>
				</div>
				
				<div class="col-sm-6 col-md-4 col-xl-3 mb-3">
					<div class="item mb-3">
						<img class="mb-3" src="assets/images/No-Claims-Unaffected_2.svg" alt="">
						<p><b>Your</b> no claims bonus won't be affected</p>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 col-xl-3 mb-3">
					<div class="item mb-3">
						<img class="mb-3" src="assets/images/Like-For-Like-Replacement_2.svg" alt="">
						<p><b>Receive</b> a like-for-like vehicle.</p>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 col-xl-3 mb-3">
					<div class="item mb-3">
						<img class="mb-3" src="assets/images/Dedicated-Claims-Handler_1.svg" alt="">
						<p><b>Dedicated</b> claims handler.</p>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 col-xl-3 mb-3">
					<div class="item mb-3">
						<img class="mb-3" src="assets/images/Avoid-Paying-Your-Excess_1.svg" alt="">
						<p><b>Avoid</b> paying your insurance excess.</p>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 col-xl-3 mb-3">
					<div class="item mb-3">
						<img class="mb-3" src="assets/images/247-Recovery-and-Storage_2.svg" alt="">
						<p><b>24/7</b> Nationwide recovery &amp; storage.</p>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 col-xl-3 mb-3">
					<div class="item mb-3">
						<img class="mb-3" src="assets/images/Manufacturer-Approved-Parts_1.svg" alt="">
						<p><b>Manufacturer-approved</b> engineers and Access to a national repair network.</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="image-top_btm-content">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="item-wrap">
						<div class="image mb-3">
							<img src="assets/images/Claims-Handler.jpg" alt="">
						</div>
						<div class="content iconpad grdar">
						<p class="regdark">To speak to an advisor for advice and guidance on getting started after a <a href="#">non-fault accident</a>, call <a href="tel:+443309128010">0330 912 8010</a>.</p>
						<p class="regdark">You can also <a href="#">start you non-fault claim online now</a>.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="item-wrap">
						<div class="image mb-3">
							<img src="assets/images/Non-Fault-Accident.jpg" alt="">
						</div>
						<div class="content iconpad grdar">
							<p class="regdark">Alternatively, you can fill out the form below and one of our New Claims Team will provide a call back to discuss the circumstances with you.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="request_form-block">
		<div class="container">
			<div class="form-wrap">
				<h3><span class="gradient-45">Request a callback.</span></h3>
				<p>One of our advisors will call you back. Or, call us now on <a href="tel:0330 165 4338">0330 165 4338</a>.</p>
				<form>
					<div class="row">
						<div class="col-sm-6">
							<div class="mb-3">
								<label for="name" class="form-label">Name</label>
								<input type="text" class="form-control" id="name">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="mb-3">
								<label for="lastname" class="form-label">last Name</label>
								<input type="text" class="form-control" id="lastname">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="mb-3">
								<label for="email" class="form-label">Email address</label>
								<input type="email" class="form-control" id="email">
								</div>
						</div>
						<div class="col-sm-6">
							<div class="mb-3">
								<label for="phone" class="form-label">Phone</label>
								<input type="text" class="form-control" id="phone">
								</div>
						</div>
					</div>

					<button type="submit" class="btn btn-primary">Submit</button>
				</form>
			</div>
		</div>
	</div>


	<?php  include 'includes/footer.php';?>

