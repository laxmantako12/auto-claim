<?php include 'includes/header.php'; ?>

<div class=" section-padding intro-block inner-banner banner-claims-management">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="text-center pt-4">
                    <p class="mb-3"><b>Independent. Trusted. Award Winning.</b></p>
                    <h1>Accident Claims <span class="gradient-45">Management</span>.</h1>
                    <img src="assets/images/claims-management/Accident-Claims-Management.webp" alt="">
                    <p class="animate delayhighslow cen click-through animated">Call our team now <a href="tel:+443301654208">0330 165 4208</a> | <a href="/non-fault-claim"><span class="ct-under">Start your claim now</span> <span class="ct-arrow">&gt;</span></a></p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end intro block  -->
<div class="container">
    <div class="incredibly_unlucky border-radius-30 bg-cover mb-4 width-1100" style="background: url(assets/images/claims-management/Blue-Background-3-1.svg);">
        <div class="row align-items-end">
            <div class="col-lg-6">
                <div class="squarebcon1 text-center">
                    <p class="boldwhite p5vh pheaders mt-5">It’s incredibly <br>unlucky to be <br> involved in any
                        <br>accident.
                    </p>
                    <p class="regwhite mb-5">When it’s one that isn’t your fault, it can be challenging to handle. Being
                        able to think clearly about what you need to do next is essential, and that is not always easy.
                        As long as the accident was not your fault, you are able to claim compensation for any injuries
                        incurred as a result of the same.</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="image-block">
                    <img src="assets/images/claims-management/Its-Unlucky-To-Be-In-Any-Accident-.webp" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end incredibly_unlucky  -->
<div class="three-steps mb-5 pb-4">
    <div class="container">
        <div class="width-1100">
            <div class="row">
                <div class="col-lg-4">
                    <div class="step-block border-radius-30 bg-cover p-40 text-white text-center mb-4" style="background: url(assets/images/claims-management/Claims-Management-Step-1.webp);">
                        <img src="assets/images/claims-management/Asset-1.svg" alt="">
                        <p class="bold p3vh">Step 1</p>
                        <p>Call the emergency services and make sure that everyone in the incident is safe and secure.
                            Stay dry, stay warm, stay safe.</p>
                    </div>
                </div>
                <!-- end col  -->

                <div class="col-lg-4">
                    <div class="step-block border-radius-30 bg-cover p-40 text-white text-center mb-4" style="background: url(assets/images/claims-management/Claims-Management-Step-2.webp);">
                        <img src="assets/images/claims-management/Asset-2.svg" alt="">
                        <p class="bold p3vh">Step 2</p>
                        <p>Straight after the incident, make notes. Make sure you collect driver details and make a
                            detailed version of events (as best as possible).</p>
                    </div>
                </div>
                <!-- end col  -->

                <div class="col-lg-4">
                    <div class="step-block border-radius-30 bg-cover p-40 text-white text-center mb-4" style="background: url(assets/images/claims-management/Claims-Management-Step-3.webp);">
                        <img src="assets/images/claims-management/Asset-3.svg" alt="">
                        <p class="bold p3vh">Step 3</p>
                        <p>Contact Auto Claims Assist immediately. We can take care of it from there! We will make sure
                            you have a replacement vehicle and anything else you need.</p>
                    </div>
                </div>
                <!-- end col  -->
            </div>
        </div>
    </div>
</div>
<!-- end 3 steps  -->
<div class="gre-box section-padding two-col-image-text-block">
    <div class="container">
        <div class="width-1100">
            <div class="row box-shodow-dark white-bg border-radius-30">
                <div class="col-lg-6">
                    <div class="image-block">
                        <img src="assets/images/claims-management/Involved-In-A-Non-Fault-Accident.webp" alt="">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="content-block text-center">
                        <h2>What if I am involved in a <br> <span class="gradient-45"> non-fault</span>car accident?
                        </h2>
                        <p><b>There are a few things that you can do immediately after an accident to prove your
                                innocence in a non-fault accident.</b></p>
                    </div>
                </div>
            </div>
            <!-- end row  -->

            <div class="row box-shodow-dark white-bg border-radius-30">
                <div class="col-lg-6">
                    <div class="image-block">
                        <img src="assets/images/claims-management/Steps-After-An-Accident.webp" alt="">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="content-block text-center">
                        <p><b><span class="gradient-45">Make a note</span> of all the vehicles involved in the
                                accident</b>, including the driver’s details, number plate and insurance details – if
                            possible.</p>
                        <p><b><span class="gradient-45">Take photos</span> with your phone so you have exact details of
                                the scene</b>, including images of the third party vehicle showing the damage incurred
                            as a result of the accident.</p>
                        <p><b><span class="gradient-45">Speak to anyone</span> who may have witnessed the accident and
                                get their details</b>, so that they can attest to the fact that you were not at fault.
                        </p>

                    </div>
                </div>
            </div>
            <!-- end row  -->

            <div class="row box-shodow-dark white-bg border-radius-30">
                <div class="col-lg-6">
                    <div class="image-block">
                        <img src="assets/images/claims-management/Contact-Auto-Claims-Assist.webp" alt="">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="content-block text-center">

                        <p><b><span class="gradient-45">Head to the hospital</span> so that your injuries can be
                                adequately treated and documented officially</b>Some injuries are superficial at first
                            glance but have long-term effects, and the hospital can consider this and note it down if
                            that is the case. You could need that information later in the accident claims process.</p>

                        <p><b><span class="gradient-45">Contact Auto Claims Assist</span></b>, - Road traffic accident
                            claims specialist – and we will deal with everything on your behalf.</p>
                        <div style="text-align: center;"><a href="/non-fault-claim" button="" class="claimcta">Start my
                                claim</a></div>

                    </div>
                </div>
            </div>
            <!-- end row  -->
        </div>
    </div>
</div>
<!-- end accident mgmt  -->



<div class="content-blocks pb-4 pt-5 mt-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="width-1100">
                    <div class="inner-content-blocks border-radius-30 p-40 text-white mb-4" style="background: url(assets/images/claims-management/Asset-1-bg.svg)">
                        <div class="width-785 pt-3 pb-3">
                            <p class="boldwhite p3vh mb-4"><b>Did you know?</b><br> <span class="regwhite">Using your
                                    own insurer could leave you worse off after a non-fault accident.</span>
                                <br><b>Making a claim can lead to increased premiums.</b>
                            </p>
                            <p>By law you have the right to choose how you claim. If you make a claim on your insurance,
                                your claim will be classed as open until all the costs have been recovered by your
                                insurer. It could take at least 12 months for your insurer to recover all their losses.
                                Your premium may increase anywhere between 20% to 50% regardless of whether it’s fault
                                or non-fault, as you are seen as higher risk. In addition this could have an effect on
                                your future policies for the next 5 years because when renewing insurers ask for your
                                claims history over this period. (Source RAC).</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end  -->
<div class="acc-mgmt-lead mt-5 min-height-1600" style="background: url(assets/images/claims-management/What-Is-Claims-Management.webp)">
    <div class="container">
        <div class="regpad">
            <h3 class="p5vh boldwhite">What exactly is <span class="gradient-pu">Claims <br>Management</span> ?</h3>

        </div>
    </div>
</div>
<!-- end  -->
<div class="black-bg text-white blb">
    <div class="container">
        <div class="width-1100">
            <div class="row">
                <div class="col-lg-6">
                    <div class="p-40"><img src="assets/images/claims-management/What-Exactly-Is-Claims-Management.webp" alt=""></div>
                </div>
                <div class="col-lg-6">
                    <div class="squarebcon1">
                        <p class="boldwhite"><b>Claims management is a crucial part of the claims process <span class="gradient-pu">after a car accident</span> and can be complex.</b> <span class="regdarktt">It involves analysing the damages, assessing claims for payment,
                                negotiating settlements and managing payments to claimants.</span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end bbl section  -->

<div class="black-bg text-white blb gutter-0">
    <div class="container">
        <div class="width-1100">
            <div class="row">
                <div class="col-lg-6">
                    <div class="squarebcon1 border-gray text-center border-bottom-0">
                        <h4 class="boldwhite pheaders" style="font-size: 35px">A dedicated
                            claims <br>manager</h4>
                        <p class="boldwhite pheaders">With <span class="gradient-pu">Auto Claims Assist</span> <br>After a <span class="gradient-pu">non-fault</span> accident.</p>
                    </div>
                    <!-- end   -->
                    <div class="squarebcon1 border-gray">
                        <p class="boldwhite"><b>Having a dedicated claims manager from Auto Claims Assist is at no cost to non-fault drivers.</b></p>
                        <p class="gradient-pu"><b>Your claims manager will help to:</b></p>
                        <ul class="custombullets regwhite text-white">
                            <li>Investigate the accident circumstances</li>
                            <li>Validate evidence &amp; advise on liability</li>
                            <li>Review and process documentation</li>
                            <li>Efficiently carry out your full claim</li>
                            <li>Develop your strategy &amp; handle communication</li>
                            <li>Keep you informed and up to date</li>
                        </ul>
                    </div>

                </div>
                <div class="col-lg-6">
                    <div class="border-gray height_100 border-left-0">
                        <div class="squarebcon1">
                            <p class="boldwhite"><b>Using extensive experience and expertise</b>, <span class="regdarktt">your claims manager will handle your claim from start-to-finish, fairly and promptly.</span></p>
                            <p class="gradient-pu"><b>An independent, trusted expert on your side.</b></p>
                        </div>
                        <img src="assets/images/claims-management/A-Dedicated-Claims-Manager.webp" alt="">
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- end bbl section  -->

<div class="black-bg text-white blb">
    <div class="container">
        <div class="width-1100">
            <div class="content-blocks border-radius-30 mb-3 text-white bg-cover pt-5 pb-5" style="background: url(assets/images/claims-management/Purple-Background-9.svg)">
                <div class="row align-items-center">
                    <div class="col-lg-5">
                        <div class="p-0 image-right width-375"><img src="assets/images/claims-management/First-Beneficial-Point-of-Contact.webp" alt="">
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="squarebcon2 whiu pb-2">
                            <p class="boldwhite pheaders p3vh">We are your first, most beneficial point of contact after an accident that wasn’t your fault.</p>
                            <p class="boldwhite"><b>As a start-to-finish accident solution, we can assist our customers immediately after the incident with roadside recovery.</b></p>
                            <p class="boldwhite"><b>We specialise in providing you with a like-for-like vehicle replacement, so you’re not left without.</b></p>
                            <p class="regwhite">Your dedicated claims manager will begin, process and finalise your claim - claiming all costs and losses from the other driver’s insurance policy.</p>
                            <p class="regwhite">As a result, the accident claim and service benefits are at no cost to you, while the process is stress-free and professionally managed for you.</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- end bbl section  -->

<div class="section-padding pt-5 gre-box accident_management_company">
    <div class="container">
        <!-- form  -->
        <div class="width-1100">
            <div class="contact-form">
                <div class="p-5 border-radius-30 box-shodow-dark white-bg">
                    <h2 class="mb-0" style="font-size: 30px;">Request a <span class="gradient-45">callback</span>.</h2>
                    <p>One of our advisors will call you back. Or, call us now on <a href="#">0330 053 2411</a>.</p>
                    <form action="">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group mb-3">
                                    <label for="">First Name</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <!-- end col  -->

                            <div class="col-lg-6">
                                <div class="form-group mb-3">
                                    <label for="">Last Name</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <!-- end col  -->

                            <div class="col-lg-6">
                                <div class="form-group mb-3">
                                    <label for="">Your Telephone</label>
                                    <input type="number" class="form-control">
                                </div>
                            </div>
                            <!-- end col  -->

                            <div class="col-lg-6">
                                <div class="form-group mb-3">
                                    <label for="">Your Email</label>
                                    <input type="email" class="form-control">
                                </div>
                            </div>
                            <!-- end col  -->

                            <div class="col-lg-12">
                                <div class="form-group mb-4 pb-1 mt-3">
                                    <button class="btn btn-default btn-graident">Request A Callback</button>
                                </div>
                            </div>
                            <!-- end col  -->
                        </div>
                    </form>
                    <p class="small"><b>This site is protected by reCAPTCHA and the Google <a href="#">Privacy Policy</a>
                            and
                            <a href="">Terms of Service</a> apply.</b></p>
                </div>
            </div>
        </div>
        <!--End form  -->
    </div>
</div>
<!-- end form  -->

<?php include 'includes/footer.php'; ?>
