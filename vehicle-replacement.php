<?php include 'includes/header.php'; ?>

<div class=" section-padding intro-block inner-banner banner-vehicle-replacement bg-cover " style="background: url(assets/images/vehicle-replacement/Like-For-Like-Replacement-Vehicle.webp);">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 offset-lg-1">
                <div class="text-center text-white">
                    <h1 class="text-white mt-0">Like-for-like <br><span class="gradient-45">replacement </span> <br>vehicle.</h1>
                    <p>So you aren’t left without the necessities if
                        the incident wasn’t your fault.</a>.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end intro block  -->
<div class="section-padding text-center white-bg replacement-block">
    <div class="container">
        <div class="width-1100">
            <div class="squarebcon1 ct-black">
                <p class="pheaders grdar" style="font-size: 35px">A <span class="gradient-45">like-for-like replacement</span> vehicle provides the necessities and comfort that you’re used to if a <a href="/non-fault-claim/">non fault accident</a> causes your vehicle to be off the road; <span class="gradient-re">leaving you without</span>.</p>
                <p class="click-through">Call us <a href="tel:+443300532411">0330 053 2411</a> | <a href="/non-fault-claim"><span class="ct-under">Start your claim now</span> <span class="ct-arrow">&gt;</span></a></p>

            </div>
            <div class="border-radius-30 bg-cover" style="background: url('assets/images/vehicle-replacement/Background-Graphic.svg')">
                <div class="image-block">
                    <img src="assets/images/vehicle-replacement/accident-vehicle-replacement-review.webp" alt="">
                </div>
                <div class="p-4">
                    <div class="squarebcon1">
                        <p class="boldwhite">“Very professional company who kept me up to date with my claim and Chris who was always willing to go the extra mile to make sure I had a hire vehicle within 3 days of the accident so I could still work.. they arrange everything for you right down to the collection of your vehicle I couldn't have asked for a better company to assist me with my vehicle claim."</p>

                        <p class="regwhite">Kieran Wood</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end section  -->
<div class="legally-entitled section-padding bg-cover" style="background: url('assets/images/vehicle-replacement/non-fault-vehicle-replacement.webp')">
    <div class="container">
        <div class="width-1100">
            <div class="row">
                <div class="col-lg-7 offset-lg-5">
                    <div class="squarebcon1 text-lg-end ">
                        <h2 class="boldwhite mt-0">You are legally entitled to a <span class="gradient-45">replacement</span> vehicle.</h2>
                        <p class="boldwhite">If yours is involved in an accident caused<br>by another party and is unroadworthy</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end section  -->
<div class="black-bg text-white blb pt-4 pb-4">
    <div class="container">
        <div class="width-1100">
            <div class="row">
                <div class="col-lg-7 offset-lg-5">
                    <div class="squarebcon1 text-lg-end ">
                        <h3 class="boldwhite p7vh">Why <span class="gradient-re">should you</span> go without?</h3>

                        <p class="boldwhite">If you choose to claim directly with your own insurer, you’ll be left with your <span class="gradient-re">excess to pay</span> and a <span class="gradient-re">basic courtesy car</span> at best.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end section  -->

<div class="black-bg text-white blb pt-4 pb-4">
    <div class="container">
        <div class="width-1100">
            <div class="row">
                <div class="col-lg-4">
                    <div class="squarebcon1">
                        <p class="boldwhite animate delaylowfast animated">An insurance company will rarely supply a vehicle with the same spec. <span class="regwhitett">Instead, a courtesy car will often come in the form of a small hatchback.</span></p>

                        <p class="boldwhite animate delaymedfast animated">For people who <span class="bolddarktt">work hard all year for their preferred car and pay increasing insurance premiums, this isn’t an acceptable outcome.</span>
                            This is where we help.</p>

                        <p class="boldwhite animate delayhighfast">As part of our Accident Management services, <span class="bolddarktt">we provide Credit Hire on a like-for-like vehicle, which is claimed back from the third-party insurer as part of your non-fault accident claim.</span></p>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="image-block">
                        <img src="assets/images/vehicle-replacement/Why-Should-You-Go-Without.webp" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end section  -->

<div class="black-bg text-white blb pt-0 pb-4">
    <div class="container">
        <div class="width-1100">
            <div class="row">
                <div class="col-lg-6">
                    <div class="squarebcon1">
                        <p class="boldwhite">You’ll have a like-for-like vehicle for the entire duration <span class="bolddarktt">of your vehicle being off the road.</span>
                        </p>
                        <p style="font-size: 30px;" class="gradient-re">No excess to pay.</p>
                        <p style="font-size: 30px;" class="gradient-re">No cost to you.</p>
                        <p style="font-size: 30px;" class="gradient-re">No interruption to your day-to-day.</p>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="squarebcon1">
                        <p class="boldwhite pheaders p4vh">Our accident management
                            specialists provide you with a like-for-like replacement, <span class="gradient-re">quickly.</span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end section  -->

<div class="black-bg text-white blb bg-cover specification-accustomed" style="background: url('assets/images/vehicle-replacement/You-Wont-Be-Left-Without-Your-Vehicle-Spec.webp')">
    <div class="container">
        <div class="width-1100">
            <div class="row">
                <div class="col-lg-8">
                    <div class="squarebcon1 p-3 text-center">
                        <p class="boldwhite pheaders p5vh">You won’t be left without the specification of a vehicle that you aren’t <span class="gradient-pu">accustomed</span> to.</p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- end section  -->

<div class="gre-box section-padding">
    <div class="container">
        <div class="width-1100">
            <div class="row">
                <div class="col-lg-8">
                    <div class="squarebcon1">
                        <p class="pheaders p7vh">Your own, <br>dedicated <span class="gradient-pu"><br>claims handler</span>.</p>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-lg-6 offset-lg-6">
                    <div class="squarebcon1">
                        <p class="regbold">We provide our customers with a dedicated claims handler during your non-fault claim. They are your point of contact if you have any questions, concerns or require an update; you won’t be alone through the process.</p>
                        <p class="regbold">Your claim handler will also be available if you need any assistance with your replacement vehicle.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end section  -->

<div class="white-bg section-padding">
    <div class="container">
        <div class="width-1100">
            <div class="row">
                <div class="col-lg-6">
                    <div class="inner-content-blocks p-40 border-radius-30 text-white mb-4 bg-cover" style="background: url('assets/images/who-we-are/Gradient-Purple-Background-6-1.svg')">
                        <div class="squarebcon1 p-0">
                            <p class="boldwhite pheaders p5vh">Auto Claims Assist are the most beneficial option after a non-fault
                                accident.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="inner-content-blocks p-40 border-radius-30 bg-cover text-white mb-4" style="background: url('assets/images/vehicle-replacement/Auto-Claims-Assist-Are-The-Most-Beneficial-Contact.webp')">
                        <div class="squarebcon1 p-0">
                            <p class="boldwhite">Established since 2009, we have developed a large fleet of vehicles, covering all specification requirements.</p>

                            <p class="boldwhite">Unlike the insurance companies, whose main objective is to save as much money as possible, our priority is our customers' comfort and needs.</p>

                            <p class="boldwhite">Our mission is to provide a service to reduce the disruprtion caused by a non-fault accident.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="gr-box200">
                        <div class="inner-content-blocks bg-cover h-750 d-flex align-items-center border-radius-30 p-40 text-white mb-4" style="background: url('assets/images/vehicle-replacement/Vehicle-Replacement-Background.webp')">
                            <div class="row">
                                <div class="col-lg-6 offset-lg-6">
                                    <div class="squarebcon1 p-0">
                                        <p class="boldwhite">We can provide you with a like-for-like vehicle (size and specification) if you are involved in a non-fault accident and will recover the cost from the third party’s insurer.</p>

                                        <p class="regwhite">You are legally entitled to a replacement vehicle if yours is involved in an accident caused by another party and is unroadworthy. </p>

                                        <p class="regwhitett">We have a large fleet of vehicles to suit all your
                                            requirements. Most are under 12 months old.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="content-blocks border-radius-30 mb-3 text-white bg-cover pt-5 pb-5" style="background: url(assets/images/claims-management/Purple-Background-9.svg)">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="squarebcon1 whiu">
                            <h3 class="boldwhite"> What happens to my vehicle?</h3>
                            <p class="regwhite">We are able to deliver vehicles nationwide to a location that suits you, delivered the same day, making your journey with us an easy one.</p>

                            <p class="regwhite">Our dedicated team of advisers will guide you through the step-by-step process removing all the stress and hassle of <a href="" style="color: #fff; text-decoration: underline;">dealing with your accident claim</a>.</p>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="p-0 text-center"><img src="assets/images/vehicle-replacement/What-Happens-To-My-Vehicle.webp" alt="">
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- end section  -->

<div class="white-bg section-padding h-750 bg-cover d-flex align-items-end" style="background: url('assets/images/vehicle-replacement/We-Claim-The-Costs-Back.webp')">
    <div class="container">
        <div class="width-1100">
            <div class="row">
                <div class="col-lg-4 offset-lg-8">
                    <div class="text-lg-end">
                        <h4 class="boldwhite">We claim <span class="gradient-45">all the costs</span> back from the other driver's insurance.</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end section  -->

<div class="gre-box section-padding pb-4 pt-4">
    <div class="container">
        <div class="width-1100">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="squarebcon1 text-center">
                        <h4 class="regbold">We keep thousands of non-fault driver's on the road, <span class="gradient-45">all year round</span>.</h4>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- end section  -->





<?php include 'includes/footer.php'; ?>
