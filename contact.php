
    <?php  include 'includes/header.php';?>

    <div class=" section-padding intro-block inner-banner squarebcon1 gre-box"
        style="background: url(assets/images/contact/Contact-us.webp);">
        <div class="container">
            <div class="text-left inner-container">
                <h1>Contact <span class="gradient-45">Auto Claims Assist</span>.</h1>
                <p><b>We are non-fault car accident claim experts.</b> Our team are the most beneficial first point
                    <br>of contact for people who have
                    been involved in a non-fault accident. <b>Get in touch with us <br> today to start your</b> <a
                        href="#">non-fault claim</a>.
                </p>
            </div>
        </div>
    </div>
    <!-- end intro block  -->
    <div class="map-form-block section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <div class="content-block mb-5">
                        <h2>Speak to us <span class="gradient-45">before your insurer</span></h2>
                        <p><b>Before you reach out to your own insurance company, or before you agree to make a
                            non-fault
                            claim with your own insurer,
                            speak to us first.</b></p>
                        <p><b>Access to our service is at no cost to you. Our specialist non-fault accident management
                            claims team are highly-trained
                            and highly-experienced in the process of road traffic accidents.</b></p>
                        <p><b>We are here to guide you through the complexities of an accident that wasn’t your fault.
                            As a
                            start-to-finish non-fault
                            accident solution, our objective is to reduce the impact and disruption that a car
                            accident
                            has on your day-to-day.</b></p>
                    </div>
                    <div class="contact-form">
                        <div class="p-5 border-radius-30 box-shodow-dark mb-4 white-bg">
                            <form action="">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group mb-3">
                                            <label for="">First Name</label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <!-- end col  -->

                                    <div class="col-lg-6">
                                        <div class="form-group mb-3">
                                            <label for="">Last Name</label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <!-- end col  -->

                                    <div class="col-lg-6">
                                        <div class="form-group mb-3">
                                            <label for="">Your Telephone</label>
                                            <input type="number" class="form-control">
                                        </div>
                                    </div>
                                    <!-- end col  -->

                                    <div class="col-lg-6">
                                        <div class="form-group mb-3">
                                            <label for="">Your Email</label>
                                            <input type="email" class="form-control">
                                        </div>
                                    </div>
                                    <!-- end col  -->
                                    <div class="col-lg-12">
                                        <div class="form-group mb-3">
                                            <label for="">Have you been involved in a road traffic accident?</label>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1">
                                                <label class="form-check-label" for="flexRadioDefault1">
                                                    Yes
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2" checked>
                                                <label class="form-check-label" for="flexRadioDefault2">
                                                    No
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end col  -->

                                    <div class="col-lg-12">
                                        <div class="form-group mb-3">
                                            <label for="">How can we help?</label>
                                            <textarea name="" id="" cols="10" rows="5" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <!-- end col  -->
                                    <div class="col-lg-12">
                                        <div class="form-group mb-3">
                                            <button class="btn btn-default btn-graident">Request A Callback</button>
                                        </div>
                                    </div>
                                    <!-- end col  -->
                                </div>
                            </form>
                            <p><small><b>This site is protected by reCAPTCHA and the Google <a href="#">Privacy Policy</a> and
                                <a href="">Terms of Service</a> apply.</b></small></p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-5">
                    <div class="map-block">
                        <div class="image-block">
                            <img src="assets/images/contact/Contact-Us-Map.svg" alt="">
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- end map-form-block  -->
    <div class="accident-shop section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="left-content-block">
                        <h2>The <span class="gradient-45">one-stop</span> non-fault <br> accident shop.</h2>
                        <p>More often than not, car accidents happen quickly and unexpectedly. It’s important to take certain steps in order for
                        you not only to feel safe but also to help to make a successful Non Fault Claim.</p>
                        <p><b>We will help you with:</b></p>
                        <ul>
                            <li>Advice and guidance on what to do after a car accident.</li>
                            <li>Help to avoid you paying your insurance excess fee.</li>
                            <li>Provide recovery of your vehicle at no cost to you.</li>
                            <li>Arrange a like-for-like replacement vehicle quickly.</li>
                            <li>Manage your full accident claim, start to finish.</li>
                            <li>Help avoid an affect on your insurance policy.</li>
                            <li>Assist with any personal injury claims.</li>
                            <li>Have your vehicle repaired and get you back on the road.</li>
                        </ul>
                        <p><b>If you’re ready to start a non-fault claim, consider starting with our non-fault claim form to begin the process.</b></p>
                        <a href="#" class="btn btn-default btn-graident">Start my claim</a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="right-content-block">
                        <div class="image-text-block  border-radius-30 box-shodow-dark mb-4 white-bg">
                            <div class="icon">
                                <img src="assets/images/contact/icons/auto-claims-assist-telephone.svg" alt="">
                            </div>
                            <div class="text-block">
                                <p>You can speak to our team at any time: <br>
                                <a href="#">0330 912 0689</a></p>
                            </div>
                        </div>
                        <!-- end block  -->
                        <div class="image-text-block  border-radius-30 box-shodow-dark mb-4 white-bg">
                            <div class="icon">
                                <img src="assets/images/contact/icons/24-7-call-centres.svg" alt="">
                            </div>
                            <div class="text-block">
                                <p>We operate our UK phone lines <b>around the clock</b>: <br>

                                Mon-Fri | 8am-10pm<br>
                                Sat | 8:30am-2:30pm<br>
                                Sun | 10am-4pm
                                </p>
                            </div>
                        </div>
                        <!-- end block  -->
                        <div class="image-text-block  border-radius-30 box-shodow-dark mb-4 white-bg">
                            <div class="icon">
                                <img src="assets/images/contact/icons/rated-excellent-icon.svg" alt="">
                            </div>
                            <div class="text-block">
                                <p>One of the UK’s most trusted accident solutions:</a>
                                </p>
                            </div>
                        </div>
                        <!-- end block  -->

                        <div class="image-text-block  border-radius-30 box-shodow-dark mb-4 white-bg">
                            <div class="icon">
                                <img src="assets/images/contact/icons/operating-nation-wide.svg" alt="">
                            </div>
                            <div class="text-block">
                                <p>We operate throughout the United Kingdom: <br>
                                Thousands of happy customers, nationwide</a>
                                </p>
                            </div>
                        </div>
                        <!-- end block  -->

                        <div class="image-text-block  border-radius-30 box-shodow-dark mb-4 white-bg">
                            <div class="icon">
                                <img src="assets/images/contact/icons/auto-claims-assist-address.svg" alt="">
                            </div>
                            <div class="text-block">
                                <p>Our head office is based in the North West: <br>
                                Atkin St, Walkden, Worsley, Manchester, M28 3DG
                                </p>
                            </div>
                        </div>
                        <!-- end block  -->

                        <div class="image-text-block  border-radius-30 box-shodow-dark mb-4 white-bg">
                            <div class="icon">
                                <img src="assets/images/contact/icons/auto-claims-assist-email.svg" alt="">
                            </div>
                            <div class="text-block">
                                <p>You can also email us directly anytime: <br>
                                    <a href="#" style="font-size:16px">newclaims@autoclaimsassist.co.uk</a>
                                </p>
                            </div>
                        </div>
                        <!-- end block  -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php  include 'includes/footer.php';?>


