
	<?php  include 'includes/header.php';?>

	<div class="intro-block squarebcon1 gre-box" style="min-height: 400px;">
		<div class="container">
			<div class="text-center">
				<h1>Road Traffic Accident <span class="gradient-45">Resource</span>.</h1>
				<p><b>Our Road Traffic Accident Resource is made up of expert industry insight for you if you’ve been
					involved <br>
					in a road traffic accident and you’re looking for all the right answers.</b></p>
			</div>
		</div>
	</div>
	<!-- end intro block  -->
	<div class="resources-block">
		<div class="container">
			<div class="row">
				<div class="col-lg-4">
					<div class="resource-block-card border-radius-30 box-shodow-dark mb-4">
						<div class="image-block">
							<img src="assets/images/Will-A-Non-Fault-Accident-Affect-My-Insurance.jpg"
								alt="">
						</div>
						<div class="content-block">
							<h6>Will A Non Fault Accident Affect My Insurance.</h6>
							<p>A non-fault accident may affect your insurance, but there are steps you can take to help
								avoid a negative outcome.</p>
						</div>
					</div>
					<div class="resource-block-card border-radius-30 box-shodow-dark mb-4">

						<div class="content-block">
							<h6>Accident Management VS Your Insurance Company.</h6>
							<p>We take you through the pro's and con's of accident management vs your insurer.</p>
						</div>
						<div class="image-block">
							<img src="assets/images/Accident-Management-Company-VS-Insurance-Company.jpg"
								alt="">
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="resource-block-card border-radius-30 box-shodow-dark mb-4">

						<div class="content-block">
							<h6>Complete Guide of What To Do In A Car Accident.</h6>
							<p>Everything you need to know for an effective non-fault claim if you've been involved in a
								car accident.</p>
						</div>
						<div class="image-block">
							<img src="assets/images/What-To-Do-In-A-Car-Accident.jpg" alt="">
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="resource-block-card border-radius-30 box-shodow-dark mb-4">

						<div class="content-block">
							<h6>How To Prove A Car Accident Wasn't Your Fault.</h6>
							<p>It is important to know how to prove a car accident wasn't your fault for a successful non-fault accident claim.</p>
						</div>
						<div class="image-block">
							<img src="assets/images/How-To-Prove-Car-Accident-Wasnt-Your-Fault.jpg"
								alt="">
						</div>
					</div>

					<div class="resource-block-card border-radius-30 box-shodow-dark mb-4">
						<div class="image-block">
							<img src="assets/images/Apple-Crash-Detection.jpg"
								alt="">
						</div>
						<div class="content-block">
							<h6>Apple Crash Detection Feature.</h6>
							<p>A short summary of the new car crash detection feature on the new iPhone & Apple Watch ranges.</p>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>

	<?php  include 'includes/footer.php';?>
