function fadeUpText() {
    const elementsToFade = document.querySelectorAll('.fade-upTxt');

    function fadeInOnScroll() {
        elementsToFade.forEach((element) => {
            const elementTop = element.getBoundingClientRect().top;
            const windowHeight = window.innerHeight;

            if (elementTop < windowHeight) {
                element.style.opacity = 1;
                element.style.transform = 'translateY(0)';
            }
        });
    }

    window.addEventListener('scroll', fadeInOnScroll);
    window.addEventListener('resize', fadeInOnScroll);

    // Initially, check if any elements are already in the viewport when the page loads.
    window.addEventListener('load', fadeInOnScroll);

}


fadeUpText();

function sameHeight () {
    // var $ = jQuery
    var options = {
        byRow: true,
        property: 'min-height',
        target: null,
        remove: false
    }
    $('.dgre-box').matchHeight(options);
    $('.inner-content-blocks').matchHeight(options);
}
sameHeight();

