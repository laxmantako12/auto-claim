<!doctype html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Auto Claim</title>
      <link href="assets/css/bootstrap.min.css" rel="stylesheet">
      <link href="assets/css/style.css" rel="stylesheet">
      <link href="assets/css/style2.css" rel="stylesheet">
   </head>
   <body>
      <nav class="navbar navbar-expand-lg">
         <div class="container">
            <a class="navbar-brand" href="./"><img src="assets/images/logo.svg" alt=""></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
               data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
               aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
               <ul class="navbar-nav mx-auto">
               <li class="nav-item dropdown dropdown-mega position-static">
                <a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown" data-bs-auto-close="outside">How We Help</a>
                <div class="dropdown-menu shadow">
                <div class="mega-content px-4">
                    <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 col-sm-4 col-md-3 py-4">
                        <h5>Pages</h5>
                        <div class="list-group">
                            <a class="list-group-item" href="#">Accomodations</a>
                            <a class="list-group-item" href="#">Terms & Conditions</a>
                            <a class="list-group-item" href="#">Privacy</a>
                        </div>
                        </div>
                        <div class="col-12 col-sm-4 col-md-3 py-4">
                        <h5>Card</h5>
                        <div class="card">
                    <img src="assets/images/Discover-accident-management.jpg" class="img-fluid" alt="image">
                    <div class="card-body">
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    </div>
                    </div>
                        </div>
                        <div class="col-12 col-sm-4 col-md-3 py-4">
                        <h5>About CodeHim</h5>
                            <p><b>CodeHim</b> is one of the BEST developer websites that provide web designers and developers with a simple way to preview and download a variety of free code & scripts.</p>
                        </div>
                        <div class="col-12 col-sm-12 col-md-3 py-4">
                        <h5>Damn, so many</h5>
                        <div class="list-group">
                            <a class="list-group-item" href="#">Accomodations</a>
                            <a class="list-group-item" href="#">Terms & Conditions</a>
                            <a class="list-group-item" href="#">Privacy</a>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </li>
                  <li class="nav-item"><a href="who-we-are.php" class="nav-link">Who we are</a></li>
                  <li class="nav-item"><a href="resources.php" class="nav-link">Resource</a></li>
                  <li class="nav-item"><a href="contact.php" class="nav-link">Contact</a></li>
               </ul>
               <ul class="navbar-nav mx-auto contact-menu">
                  <li class="nav-item">
                     <a class="nav-link" href="tel:0330 053 2411">0330 053 2411</a>
                  </li>
                  <li>
                     <a class="claimcta" href="start-my-claim.php">Start my claim</a>
                  </li>
               </ul>
            </div>
         </div>
      </nav>


<!-- END Mega Menu HTML -->
      <div class="main-site">
