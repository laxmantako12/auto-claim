    </div>
    <!-- site-wrapper ends -->
    <div class="site-footer">
    	<div class="footer-main">
    		<div class="container">
    			<div class="row">
    				<div class="col-md-3">
    					<div class="footer-logo mb-2">
    						<a href="./"><img src="assets/images/logo.svg" alt=""></a>
    					</div>
    					<p class="footercont">Auto Claims Assist Ltd <br> Atkin Street<br> Walkden<br> M28 3DG<br> <br><a href="mailto:newclaims@autoclaimsassist.co.uk">newclaims@autoclaimsassist.co.uk</a><br> <a href="tel:+443309128010">0330 912 8010</a><br> Registered: 06852246<br> <a href="https://www.fca.org.uk/" target="_blank" rel="nofollow">FCA Regulated</a>: FRN830899</p>
    				</div>
    				<div class="col-md-3">
    					<div class="footer-menu">
    						<h4 class="footerhead">How we help</h4>
    						<ul>
    							<li><a href="accident-management.php">Accident Management</a></li>
    							<li><a href="claims-management.php">Claims Management</a></li>
    							<li><a href="#">Road Traffic Accident Claims</a></li>
    							<li><a href="#">Non Fault Claim</a></li>
    							<li><a href="credit-hire.php">Credit Hire</a></li>
    							<li><a href="#">Replacement Vehicle</a></li>
    							<li><a href="#">Accident Recovery</a></li>
    							<li><a href="#">Car Accident Repair</a></li>
    							<li><a href="#">Personal Injury Claim</a></li>
    							<li><a href="#">Bicycle Accident Claims</a></li>
    						</ul>
    					</div>
    				</div>
    				<div class="col-md-3">
    					<div class="footer-menu">
    						<h4 class="footerhead">How we help</h4>
    						<ul>
    							<li><a href="#">What to do in a car accident</a></li>
    							<li><a href="#">ACA VS your insurer</a></li>
    							<li><a href="#">Proving it wasn’t your fault</a></li>
    							<li><a href="#">Affecting your insurance</a></li>
    							<li><a href="#">Avoid your insurance excess</a></li>
    							<li><a href="#">Protect your no claims bonus</a></li>
    							<li><a href="#">Side impacts collisions</a></li>
    							<li><a href="#">Rear-ended collisions</a></li>
    							<li><a href="#">All accident resources</a></li>
    						</ul>
    					</div>
    				</div>
    				<div class="col-md-3">
    					<div class="footer-menu">
    						<h5 class="footerhead">Auto Claims Assist</h5>
    						<ul>
    							<li><a href="#">Review us on Trustpilot</a></li>
    							<li><a href="#">Review us on Google</a></li>
    							<li><a href="#">About our company</a></li>
    							<li><a href="#">Our accident experts</a></li>
    							<li><a href="#">Accident repair centre</a></li>
    							<li><a href="#">Contact us</a></li>
    							<li><a href="#">Start a non-fault claim</a></li>
    						</ul>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    	<div class="footer-secondary">
    		<div class="container">
    			<div class="row">
    				<div class="col-xl-6">
    					<div class="footercont">Copyright ⓒ 2023 | Auto Claims Assist Ltd | <a href="https://www.disruptcreative.agency" target="_blank">Website by DISRUPT.</a></div>
    				</div>
    				<div class="col-xl-6">
    					<div class="footercont"><a href="/privacy-policy/">Privacy Policy</a> | <a href="/privacy-policy/">Cookie Policy</a> | <a href="/privacy-policy/">Terms &amp; Conditions</a> | <a href="/sitemap_index.xml">Site Map</a> | <a href="/our-locations/">Locations</a></div>
    				</div>
    			</div>

    		</div>
    	</div>
    </div>
    <script src="assets/js/jquery-3.7.1.min.js"></script>
    <script src=" https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous"></script>
    <script src="assets/js/jquery.matchHeight-min.js"></script>
    <script src="assets/js/scripts.js"></script>
    </body>

    </html>
