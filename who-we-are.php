<?php  include 'includes/header.php';?>

<div class=" section-padding intro-block inner-banner banner-who-we-are "
    style="background: url(assets/images/who-we-are/Accident-Management-Company.webp);">
    <div class="container">
        <div class="row">
            <div class="col-md-5 offset-md-7">
                <div class="text-center">
                    <h1>Our Accident <span class="gradient-45">Management </span>Company.</h1>
                    <p><b>We are non-fault car accident claim experts.</b> Our team are the most beneficial first point
                        <br>of contact for people who have
                        been involved in a non-fault accident. <b>Get in touch with us <br> today to start your</b> <a
                            href="#">non-fault claim</a>.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end intro block  -->
<div class="Auto-Claims-Assist pb-4 section-padding">
    <div class="container">
        <p class="p7vh pheaders mb-5">We are <span class="gradient-45">Auto Claims Assist</span>.</p>
        <div class="row mb-3">
            <div class="col-lg-6">
                <p><b>An Accident Management Company that operates throughout the United Kingdom with our head office
                        based in Manchester.</b></p>
                <p><b>We provide start-to-finish <a href="#">claims management services</a></b> <span class="regdarktt">
                        to help and support people throughout the UK who have been
                        involved in a non-fault road traffic accident.</span></p>
            </div>
            <div class="col-lg-6 text-center">
                <img src="assets/images/who-we-are/Auto-Claims-Assist-Logo.svg" style="height: 100px;" alt="">
            </div>
        </div>
        <div class="mt-5 pt-3"></div>
        <div class="text-center mb-5">
            <p><b>We’re proud to say that we are leading Accident Management Specialists for <a href="#">Road Traffic
                        Accident Claims</a>.</b></p>
        </div>
        <div class="mb-2 pb-2"></div>
        <div class="embed-responsive embed-responsive-16by9 mb-5">
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0"
                allowfullscreen></iframe>
        </div>
    </div>
</div>
<!-- end We are Auto Claims Assist  -->
<div class="gre-box section-padding two-col-image-text-block">
    <div class="container">
        <div class="row box-shodow-dark white-bg border-radius-30">
            <div class="col-lg-6">
                <div class="image-block">
                    <img src="assets/images/who-we-are/Auto-Claims-Assist-Who-We-Are.webp" alt="">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="content-block text-center">
                    <h2>Auto Claims Assist - <span class="gradient-45">Who we are</span>.</h2>
                    <p><b>Auto Claims Assist Ltd are a national Accident Management Company who were founded in
                            2009.</b></p>
                    <p>Our skilled team has over 30 years of experience in the complexities of Accident Claims
                        Management.</p>
                </div>
            </div>
        </div>
        <!-- end row  -->

        <div class="row box-shodow-dark white-bg border-radius-30">
            <div class="col-lg-6">
                <div class="image-block">
                    <img src="assets/images/who-we-are/Our-Mission.webp" alt="">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="content-block ">
                    <h2>Our <span class="gradient-45">Mission</span>.</h2>
                    <p><b>Our mission is to help you get back on the road as quickly and as easily as possible after a
                            road traffic accident.</b></p>
                    <p>To accomplish this we:</p>
                    <ul>
                        <li><a href="#">Recover your car</a></li>
                        <li><a href="#">Provide like-for-like Replacement</a></li>
                        <li><a href="#">Repair your car</a></li>
                        <li><a href="#">Manage your entire Accident Claim Process</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- end row  -->

        <div class="row box-shodow-dark white-bg border-radius-30">
            <div class="col-lg-6">
                <div class="image-block">
                    <img src="assets/images/who-we-are/Our-Accident-Management-Company.webp" alt="">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="content-block ">
                    <h2>Our <span class="gradient-45">Accident</span> <br>Management Company.</h2>
                    <p><b>Our Accident Management Company ethos is based on giving you the best customer experience
                            possible.</b></p>
                    <p>Things can be challenging after a road traffic accident.</p>
                    <p>This is why we provide around the clock assistance to help you, should you be unfortunate enough
                        to be involved in a car
                        crash.</p>
                </div>
            </div>
        </div>
        <!-- end row  -->
    </div>
</div>
<!-- end accident mgmt  -->
<div class="white-bg section-padding inner-banner road-traffic-accident" style="background: url(assets/images/who-we-are/Best-Option-is-an-Accident-Management-Company.webp);background-position: right center;
  background-size: 80% !important;">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-lg-7">
                <div class="title-block">
                    <h2>After a non-fault <span class="gradient-45">road traffic accident</span>, the best option for
                        you will always be
                        an Accident Management Company.</h2>
                </div>
                <div class="content">
                    <p><b>This is because the claims management services are provided independently to your insurer,
                            giving you full control of
                            the situation.</b></p>
                    <p><b>Our promise to you, as an Accident Management Company, is that we’ll provide the most
                            reliable, informed and best value
                            service to you, in this time of need.</b></p>
                    <p>Browse the services that we provide to you after a non-fault accident:</p>
                    <ul>
                        <li>Accident Recovery</li>
                        <li>Like-for-like vehicle replacement</li>
                        <li>Accident claims management</li>
                        <li><a href="#">Personal injury</a></li>
                    </ul>
                    <div class="light-text">
                        <p><b>If you’re still not sure what an accident management company is or what accident
                                management specialists do, allow us to
                                give <a href="#">you a callback</a> or continue reading through this page for more
                                information on an Accident Management Company and
                                more about Auto Claims Assist.</b></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end accident mgmt  -->
<div class="section-padding gre-box accident_management_company">
    <div class="container">
        <div class="title-block text-center mb-5 pb-5">
            <h2 class="mt-0">What does an <span class="gradient-45">Accident Management</span> <br> Company do?</h2>
        </div>
        <div class="accident_management_company_content-section">
            <div class="row">
                <div class="col-lg-4">
                    <div class="column-block">
                        <img src="assets/images/who-we-are/What-Does-Accident-Management-Company-Do-.webp" alt="">
                    </div>
                </div>
                <div class="col-lg-4 middle">
                    <div class="p-4 column-block box-shodow-dark white-bg border-radius-30 mb-3">
                        <p><b><span class="gradient-45">The idea of an accident management company</span> at this point
                                may sound intriguing but you might be not entirely sure what they do.</b></p>
                        <p><b>Put simply, we handle EVERYTHING for you after a non-fault accident at NO COST to you.</b>
                        </p>
                        <p>Our daily objective is to assist a non-fault accident party (you) get back on the road as
                            quickly as possible after your road traffic accident.</p>
                        <p>We are completely independent and our only responsibility is to you, our client.</p>

                        <div class="light-text">
                            <p><b>The main function of an Accident Management Company is to manage the full accident
                                    claim from start to finish.</b></p>
                        </div>
                    </div>
                    <!-- end block  -->
                    <div class="p-4 column-block box-shodow-dark white-bg border-radius-30 mb-3">
                        <p><b><span class="gradient-45">Our team are experts</span> at dealing with all parties and
                                organisations involved.</b></p>
                        <p>We’ve developed streamlined Accident management processes which stop any unnecessary delays
                            or issues, but our processes ensure that your process leaves no stone unturned.</p>

                        <div class="light-text">
                            <p><b>If end-customers try to manage claims themselves, there are many aspects that may not
                                    get considered when it comes to:</b></p>
                            <ul>
                                <li>Provide a like-for-like vehicle</li>
                                <li>Recovering costs.</li>
                                <li>Covering damages and losses.</li>
                                <li>Organising thorough repairs.</li>
                                <li>Reports and certificates.</li>
                                <li>Knowing what to watch out for.</li>
                            </ul>
                        </div>
                    </div>
                    <!-- end block  -->
                </div>

                <div class="col-lg-4">
                    <div class="column-block">
                        <img src="assets/images/who-we-are/What-Does-An-Accident-Management-Company-Do-.webp" alt="">
                    </div>
                </div>


            </div>
        </div>
        <!-- end  -->
        <!-- form  -->
        <div class="contact-form pt-5 mt-5">
            <div class="p-5 border-radius-30 box-shodow-dark white-bg">
                <h2 class="mb-0" style="font-size: 30px;">Request a <span class="gradient-45">callback</span>.</h2>
                <p>One of our advisors will call you back. Or, call us now on <a href="#">0330 053 2411</a>.</p>
                <form action="">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group mb-3">
                                <label for="">First Name</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <!-- end col  -->

                        <div class="col-lg-6">
                            <div class="form-group mb-3">
                                <label for="">Last Name</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <!-- end col  -->

                        <div class="col-lg-6">
                            <div class="form-group mb-3">
                                <label for="">Your Telephone</label>
                                <input type="number" class="form-control">
                            </div>
                        </div>
                        <!-- end col  -->

                        <div class="col-lg-6">
                            <div class="form-group mb-3">
                                <label for="">Your Email</label>
                                <input type="email" class="form-control">
                            </div>
                        </div>
                        <!-- end col  -->

                        <div class="col-lg-12">
                            <div class="form-group mb-4 pb-1 mt-3">
                                <button class="btn btn-default btn-graident">Request A Callback</button>
                            </div>
                        </div>
                        <!-- end col  -->
                    </div>
                </form>
                <p class="small"><b>This site is protected by reCAPTCHA and the Google <a href="#">Privacy Policy</a>
                        and
                        <a href="">Terms of Service</a> apply.</b></p>
            </div>
        </div>
        <!--End form  -->
    </div>
</div>
<!-- end form  -->
<div class="accident-management-company section-padding white-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 mb-5">
                <h2 class="p7vh pheaders">Choosing between an <span class="gradient-pu"> Accident Management
                        Company</span> or the insurance <br> company.</h2>
            </div>
        </div>
        <div class="row">
            <div class="offset-lg-7 col-lg-5">
                <p><b>Insurance companies have their own methods of approaching accident situations. Unfortunately for
                        the non-fault driver involved, these methods do not prioritise your wellbeing or vehicle
                        requirements.</b>

                </p>
                <p>These methods will usually have cost savings very high on the agenda.</p>
                <div class="light-text">
                    <p>

                        <b>But why is this fair on the non-fault driver? Who pays consistent insurance premiums
                            year-on-year?</b>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end Accident Management Company  -->
<div class="content-blocks pb-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="inner-content-blocks p-40 border-radius-30 text-white mb-4"
                    style="background: url(assets/images/who-we-are/Gradient-Purple-Background-7.svg)">
                    <p><b>As Accident Management Specialists, we ensure that you receive everything that you’re entitled
                            to, without any corners cut based on cost-savings.</b></p>
                    <p>As an independent Accident Management Company, we handle your case fairly. But most importantly;
                        at no cost to you.</p>
                    <p><b>The reality is; an Accident Management Company does not benefit from the attempts at
                            cost-cutting from the insurer's side of the process.</b></p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="inner-content-blocks p-40 border-radius-30 black-bg text-white mb-4">
                    <p><b>As a result, an accident management company is 100% on your (the non-fault parties) side.</b>
                    </p>
                    <p>Focused on ensuring that your experience is fully inclusive of everything you need during your
                        downtime.</p>
                    <p class="boldwhitett"><b>One common mistake that people make after they’ve had a non-fault accident
                            is to instantly ring their insurance provider to discuss their options.</b></p>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="inner-content-blocks border-radius-30 p-40 text-white mb-4"
                    style="background: url(assets/images/who-we-are/Gradient-Purple-Background-6-1.svg)">
                    <p><b>Whilst phoning your insurer may seem logical, it is not beneficial to the non-fault party. If
                            anything, quite the opposite.</b></p>
                    <p><b>You risk losing your no claims bonus.</b> <br>
                        Even if the accident wasn’t your fault, you risk having to make a claim and paying excess.</p>
                    <p><b>You risk higher insurance premiums.</b> <br>
                        Until the claim is finalised, your insurance policy will have an on-going claim. It’s likely
                        that your next insurance premium renewal will be due during this process and you will experience
                        price increases for this.</p>
                    <p><b>You won’t receive a like-for-like replacement vehicle.</b> <br>
                        Many drivers will be given a vehicle of a different size or spec to their own. This can be
                        impractical for their vehicle needs.</p>
                    <p><b>You may not get a say in your repair.</b> <br>
                        Your insurance company may decide to cost-cut during the repair of your vehicle. The garage and
                        parts may not be most suited for your vehicle.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end  -->
<div class="acc-mgmt-lead mt-5" style=" background: url(assets/images/who-we-are/Using-an-Accident-Management-Company.webp)">
    <div class="container">
        <div class="regpad">
            <h3 class="p5vh boldwhite">Using an <span class="gradient-pu">Accident Management</span> <br>Company will
                lead to:</h3>
            <ul class="custombullets regwhite">
                <li>A better end result.</li>
                <li>A professionally managed service.</li>
                <li>The best experience for you.</li>
                <li>Overall less stress.</li>
            </ul>
        </div>
    </div>
</div>
<!-- end  -->
<div class="black-bg text-white blb">
    <div class="container">
        <div class="regpad">
            <h3 class="p4vh boldwhite pheaders">The Accident Management<br> Company <span
                    class="gradient-pu">Process</span>.</h3>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="p-40"><img src="assets/images/who-we-are/The-Accident-Management-Company-Process.webp" alt=""></div>
            </div>
            <div class="col-lg-6">
                <div class="sidepad">
                    <p class="boldwhite"><b>The accident management process with Auto Claims Assist is really
                        straightforward.</b></p>
                    <p class="boldwhite"><b>It is way easier than dealing with the process on your own.</b></p>
                    <p class="boldwhitett"><b>The benefit of being accident management specialists since 2009 is that we’ve
                        developed a journey that is:</b></p>
                    <ul class="custombullets regwhitett">
                        <li>Smooth</li>
                        <li>Hassle-free</li>
                        <li>Reliable</li>
                        <li>Professional</li>
                        <li>Experienced</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end bbl section  -->

<div class="black-bg text-white blb">
    <div class="container">
        <div class="regpad">
            <h3 class="p4vh boldwhite pheaders">We want to make things as easy <br> as possible <span class="gradient-pu">for you</span>, the client.</h3>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="p-40">
                    <img src="assets/images/who-we-are/We-Want-To-Make-Things-Easy.webp" alt="">
                </div>
            </div>
            <div class="col-lg-6">
            <div class="squarebcon1 p-40">
                <p class="gradient-pu bold">Our accident management specialists operate across the UK.</p>
                <p class="boldwhite bold">We have an established network of repair garages to effectively manage your claim after a road
                    traffic accident.</p>
                <p class="boldwhite bold">The three main steps to our accident management client journey are:</p>
                <p class="regwhite">1. Get In Touch<br> 2. Claims Set Up &amp; Management<br> 3. Roadside Recovery<br> 4. Vehicle
                    Replacement (Like-for-like)<br> 5. Vehicle Storage<br> 6. Vehicle Repair &amp; Management<br> 7. Personal
                    Injury<br> 8. Back On The Road</p>
                <p class="boldwhitett bold">We pride ourselves on our easy to understand services.</p>
                <p class="boldwhitett bold">Our customers love that we are genuine and personal accident management specialists that put
                    their needs first.</p>
            </div>
            </div>
        </div>
    </div>
</div>
<!-- end bbl section  -->
<div class="graident-line black-bg">
    <div class="container">
    <img src="assets/images/who-we-are/Gradient-Separator.svg" alt="">
    </div>
</div>
<!-- graident line  -->

<div class="black-bg text-white blb">
    <div class="container">

        <div class="row">
            <div class="col-lg-6">
            <div class="regpads">
            <p class="gradient-pu bold mb-4">Accident Management Company Process</p>
            <h3 class="p4vh boldwhite pheaders">Getting in touch and Accident Claims Management.</h3>
        </div>
                <div class="pt-1 p-4 pl-0">
                    <img src="assets/images/who-we-are/Getting-In-Touch.webp" alt="">
                </div>
            </div>
            <div class="col-lg-6">
            <div class="squarebcon1 p-0">
                <p class="bold">Being involved in a non-fault accident is an unfortunate and challenging situation.</p>

                <p class="boldwhite bold">It’s very important in the moment of a crash to try and keep your head clear and focus on getting all the information you need.</p>

                <p class="regwhite">You should still call the emergency services even if no one sufferers a severe injury.</p>
                <p class="regwhite">Once you’ve checked that everyone is okay, you need to be certain that the accident wasn’t your fault.</p>

                <p>With everyone safe and the situation calmed, get in touch with the accident management specialists at Auto Claims Assist.</p>

                <p class="boldwhitett bold">We will open the claim by taking all of the relevant details.</p>
                <p class="boldwhitett bold">From here, you’ll be assigned a dedicated claim handler to manage your claim.</p>
            </div>
            </div>
        </div>
    </div>
</div>
<!-- end bbl section  -->
<div class="graident-line black-bg">
    <div class="container">
    <img src="assets/images/who-we-are/Gradient-Separator.svg" alt="">
    </div>
</div>
<!-- graident line  -->

<div class="black-bg text-white blb">
    <div class="container">

        <div class="row">
            <div class="col-lg-6">
            <div class="regpads">
            <p class="gradient-pu bold mb-4">Accident Management Company Process</p>
            <h3 class="p4vh boldwhite pheaders">Accident Repair.</h3>
        </div>
                <div class="pt-1 p-4 pl-0">
                    <img src="assets/images/who-we-are/Recovery-After-An-Accident.webp" alt="">
                </div>
            </div>
            <div class="col-lg-6">
            <div class="squarebcon1 p-0">
            <p class="boldwhite bold">Our <span class="gradient-pu">Accident Recovery</span> team will then make their way to you promptly.</p>
            <p class="regwhite">The priority will be to move your vehicle to a safe place using a careful process.</p>
            <p class="regwhite">We will then clean up the scene, collecting any car parts and debris.</p>
            <p class="boldwhitett">Once the scene is cleared, we will tow your vehicle away to the nearest, safest location.</p><p class="boldwhitett bold">From here, we will organise full storage of your vehicle until it’s ready for repairs.</p></div>

            </div>
        </div>
    </div>
</div>
<!-- end bbl section  -->
<div class="graident-line black-bg">
    <div class="container">
    <img src="assets/images/who-we-are/Gradient-Separator.svg" alt="">
    </div>
</div>
<!-- graident line  -->

<div class="black-bg text-white blb">
    <div class="container">

        <div class="row">
            <div class="col-lg-6">
            <div class="regpads">
            <p class="gradient-pu bold mb-4">Accident Management Company Process</p>
            <h3 class="p4vh boldwhite pheaders">Like-for-like <br> replacement.</h3>
        </div>
                <div class="pt-1 p-4 pl-0">
                    <img src="assets/images/who-we-are/Like-For-Like-Replacement.webp" alt="">
                </div>
            </div>
            <div class="col-lg-6">
            <div class="squarebcon1 p-0">
                <p class="boldwhite bold">The very first concern you have right now is “…but I need a car to use!".</p>

                <p class="bold">Our <span class="gradient-pu bold">Vehicle replacement</span>  is a service that we specialise in.</p>

                <p class="regwhite">By choosing to use an Accident Management Company, you’ll be guaranteed a vehicle with a like-for-like specification to your own.</p>

                <p>Going directly, insurance companies will make attempts to avoid offering this level of service.</p>

                <p class="boldwhitett bold">As part of your claims management, we will process the costs of a like-for-like replacement through the at-fault parties' insurance.</p>
                <p class="boldwhitett bold">We will have the like-for-like replacement to you from within 3 hours of your call to us.</p>
            </div>
            </div>
        </div>
    </div>
</div>
<!-- end bbl section  -->
<div class="graident-line black-bg">
    <div class="container">
    <img src="assets/images/who-we-are/Gradient-Separator.svg" alt="">
    </div>
</div>
<!-- graident line  -->

<div class="black-bg text-white blb">
    <div class="container">

        <div class="row">
            <div class="col-lg-6">
            <div class="regpads">
            <p class="gradient-pu bold mb-4">Accident Management Company Process</p>
            <h3 class="p4vh boldwhite pheaders">Accident Repair.</h3>
        </div>
                <div class="pt-1 p-4 pl-0">
                    <img src="assets/images/who-we-are/Accident-Repair.webp" alt="">
                </div>
            </div>
            <div class="col-lg-6">
            <div class="squarebcon1 p-0">
                <p class="boldwhite bold">The next priority is to get your damaged vehicle repaired after an accident.</p>

                <p class="bold">We don’t want the repair process to cause any delays.</p>

                <p class="regwhite bold">So to create a fluid process in accident repairs, we have established a 400+ trusted repair garage network./p>

                <p>This allows us to provide local repairs for you, but to ensure we aren’t restricted by busy garages and long wait times.</p>

                <p class="boldwhitett">Our repair network consists of specialist garages, manufacturer-approved garages and highly trusted garages. For example, if you have an accident in a BMW, we will ensure that new and genuine <span class="gradient-pu">BMW parts are used during the repair</span>.</p>
                <p class="boldwhitett bold">We’re able to find the perfect fit, no matter what damage has occurred or type of vehicle you have.</p>
                <p class="boldwhitett bold">Your dedicated claims handler will keep you up to date throughout the repair process.</p>
            </div>
            </div>
        </div>
    </div>
</div>
<!-- end bbl section  -->
<div class="graident-line black-bg">
    <div class="container">
    <img src="assets/images/who-we-are/Gradient-Separator.svg" alt="">
    </div>
</div>
<!-- graident line  -->


<div class="black-bg text-white blb">
    <div class="container">

        <div class="row">
            <div class="col-lg-6">
            <div class="regpads">
            <p class="gradient-pu bold mb-4">Accident Management Company Process</p>
            <h3 class="p4vh boldwhite pheaders">Back on the road & Personal Injury Claims.</h3>
        </div>
                <div class="pt-1 p-4 pl-0">
                    <img src="assets/images/who-we-are/Back-on-the-road.webp" alt="">
                </div>
            </div>
            <div class="col-lg-6">
            <div class="squarebcon1 p-0">

                <p class="boldwhite bold">At this point, your vehicle is repaired and everything is going smoothly with your accident claims management.</p>
                <p class="boldwhite bold">You will receive a full vehicle accident report.</p>
                <p class="boldwhite bold">This will give you all the details of your vehicle's repair and current status.</p>
                <p class="boldwhite bold">Your vehicle’s safety will always be the No.1 priority at this stage.</p>

                <p class="regwhite">We want you to feel safe knowing that you are driving a safe and secure vehicle.</p>

                <p>We’ll hand back the keys and the process will be complete.</p>

                <p class="boldwhitett bold">In addition to our general accident claims management, we also have a panel of partnered solicitors.</p>
                <p class="boldwhitett bold">We work with our specialist personal injury solicitors to organise <span class="gradient-pu bold">personal injury claims that you’re entitled to</span>.</p>
            </div>
            </div>
        </div>
    </div>
</div>
<!-- end bbl section  -->



<div class="black-bg text-white  pb-4 pt-5 ">
    <div class="container mb-4 mt-5">
    <div class="accordion" id="accordionExample">
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingOne">
      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
      Do Auto Claims Assist operate near me?
      </button>
    </h2>
    <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
      <div class="accordion-body">
        <b>Our accident claims management services cover the entire UK. <br>

This includes Accident Recovery, Accident Repair and Accident Replacement. <br>

We’re proud to be able to offer you a fast, convenient and reliable service no matter where you are. </b>
      </div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingTwo">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
      How do I know if an accident wasn’t my fault?
      </button>
    </h2>
    <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
      <div class="accordion-body">
        <strong>This is the second item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the accordion-bod, though the transition does limit overflow.
      </div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingThree">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
      Is claims management regulated by the FCA?
      </button>
    </h2>
    <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
      <div class="accordion-body">
        <strong>This is the third item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the accordion-body, though the transition does limit overflow.
      </div>
    </div>
  </div>
</div>
    </div>
</div>
<?php  include 'includes/footer.php';?>
