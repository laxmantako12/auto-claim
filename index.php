
	<?php  include 'includes/header.php';?>

	<div class="intro" style="background-image: url('assets/images/Non-Fault-Accident-Experts.jpg');">
		<div class="container">
			<div class="row">
				<div class="col-xl-7">
					<div class="regpad ct-black text-center">
					<h1 class="cen"> No Excess.<br> Policy <span class="gradient-45">Unaffected</span>.<br> Like-for-like Vehicle.</h1>
					<p class="cen">An independent, trusted, award-winning accident management service at no cost to you and on your side if you’ve had a car accident.</p> 
					<p class="cen click-through">Call our new claims team on <a href="tel:+443309128010">0330 912 8010</a> | <a href="/non-fault-claim"><span class="ct-under">Start your claim now</span> <span class="ct-arrow">&gt;</span></a></p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="work-days">
		<div class="container">
			<div class="big-title">
				<p class="p4vh pheaders">For our experts</p>
				<div class="text">
					<p class="gradient-180 aiadw pheaders">It's all in a</p>
				</div>
				<div class="rig">
					<p class="gradient-180 aiadw pheaders">Days Work</p>
				</div>
				
			</div>
			<div class="row">
				<div class="col-xl-7 offset-xl-3 mb-3">
					<div class="info">
						<p>We set up and manage your non-fault claim after an accident that wasn’t your fault and have large fleet of vehicles, ready so you aren’t left without. <span class="gradient-180">At no cost to you.</span></p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 mb-3">
					<div class="content">
						We <span class="gradient-45">Provide</span>
						<br>a vehicle from within
						<br>4 hours of an accident.
					</div>
				</div>
				<div class="col-md-4 mb-3">
					<div class="content">
						We <span class="gradient-45">Claim</span>
						<br>all costs back from <br>the other drivers insurance.
					</div>
				</div>
				<div class="col-md-4 mb-3">
					<div class="content">
						We <span class="gradient-45">Manage</span>
						<br>a vehicle from within<br> 4 hours of an accident.
					</div>
				</div>
				
			</div>
		</div>
	</div>

	<div class="better-section">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="box-content d-flex align-items-end mb-4" style="background-image: url('assets/images/Discover-accident-management.jpg');">
						<div class="content squarebcon1 ct-white">
							<h2 class="boldwhite">Discover<br>
								Accident<br>
								<span class="gradient-45">Management</span>.</h2>
								<p class="click-through ct-light"><a href="#"><span class="ct-under">How we can help you</span></a> <span class="ct-arrow">&gt;</span></p>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="box-content d-flex align-items-start mb-4" style="background-image: url('assets/images/Dedicated-Accident-Claims-Handler-2.jpg');">
						<div class="content squarebcon1 ct-white">
							<h2 class="boldwhite">Discover<br>
								Accident<br>
								<span class="gradient-45">Management</span>.</h2>
								<p class="click-through ct-light"><a href="#"><span class="ct-under">Start your claim now</span></a> <span class="ct-arrow">&gt;</span></p>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="box-content d-flex align-items-center mb-3 text-center">
						<div class="content white-bg">
							<h2>The better option after a <br> <span class="gradient-45">non-fault</span> accident.</h2>
							<p>Driver’s think to contact their insurance after an <br> accident that wasn’t their fault. This isn’t the best option. <br> We are your non-fault claims solution.</p>
							<a href="#"  class="claimcta">Start my claim</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="top-content_btmimg">
		<div class="container">
			<div class="row">
				<div class="col-md-8 mb-4">
					<div class="content">
						<h2 class="">Contact <span class="gradient-45">Auto Claims Assist</span> first after an accident. Don’t contact your own insurer.<br></h2>
						<p><b>Some people believe that the first thing they must do</b> after a non-fault accident is to call their own insurance company. Using your own insurer <span class="gradient-45"><strong>could leave you worse off</strong></span> if the accident wasn’t your fault.</p>
					</div>
				</div>
				<div class="col-md-8 offset-md-4">
					<div class="img">
						<img src="assets/images/contact-an-accident-management-company-first.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="content-contentlist_right">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-xl-6">
					<div class="content-left">
						<p><b>By contacting Auto Claims assist first</b>, you’ll be <span class="gradient-45"><strong>entitled to a much better experience</strong></span> and at no cost to you. We claim back all costs from the at-fault driver’s insurance and <b>leave your own insurance policy untouched</b>.</p>
					</div>
				</div>
				<div class="col-md-6 col-xl-5 offset-xl-1">
					<div class="content-list">
						<ul>
							<li>
								<span class="icon">
									<img src="assets/images/car.png" alt="">
								</span>
								<span class="content">
									<span class="gradient-45">Replacement vehicle</span><br><span class="listtext">Like-for-like while yours is off the road</span>
								</span>
							</li>
							<li>
								<span class="icon">
								<img src="assets/images/money.png" alt="">
								</span>
								<span class="gradient-45">No excess to pay</span><br><span class="listtext">Avoid paying your policy claim excess</span>
							</li>
							<li>
								<span class="icon">
								<img src="assets/images/claims.png" alt="">
								</span>
								<span class="gradient-45">Protect your no claims</span><br><span class="listtext">Your no claims bonus won’t be affected</span>
							</li>
							
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="large-text lg">
		<div class="container">
			<ul>
				<li class="fade-upTxt"><span class="hmg1">Rated ‘Excellent’ on Trustpilot.</span></li>
				<li class="fade-upTxt"><span class="hmg2">Award-winning and FCA regulated.</span></li>
				<li class="fade-upTxt"><span class="hmg3">Your own dedicated claims handler.</span></li>
				<li class="fade-upTxt"><span class="hmg4">Independent engineers.</span></li>
				<li class="fade-upTxt"><span class="hmg5">24/7 Accident recovery.</span></li>
				<li class="fade-upTxt"><span class="hmg6">Like-for-like replacement vehicle.</span></li>
				<li class="fade-upTxt"><span class="hmg7">No excess.</span></li>
				<li class="fade-upTxt"><span class="hmg8">No NCB loss.</span></li>
				<li class="fade-upTxt"><span class="hmg9">No insurance increase.</span></li>
				<li class="fade-upTxt"><span class="hmg10">No cost to you.</span></li>
				<li class="fade-upTxt"><span class="hmg11">No hassle.</span></li>
			</ul>
		</div>
	</div>
	<div class="help-block" style="background-image: url('assets/images/We-help-thousands-of-people-after-car-accidents.jpg');">
		<div class="container">
			<div class="squarebcon1"><h3 class="cen boldwhite">We help <span class="gradient-pu">thousands of people</span>, <br>all year round, after an accident.</h3> <br> <br>
				<p class="cen boldwhite">Start your claim today with one of the <span class="gradient-pu">UK’s most trusted</span><br> Accident Management Companies.</p> <br><div style="text-align: center;"><a href="/non-fault-claim"  class="claimcta">Start my claim</a>
				</div>
			</div>
		</div>
	</div>
	<div class="bg-black-text blb">
		<div class="container">
			<div class="row">
				<div class="col-xl-8 offset-xl-2 text-center">
					<h3 class="boldwhite cen p5vh">Starting a <span class="gradient-pu">non-fault claim </span> <br>with Auto Claims Assist.</h3>
				</div>
				
			</div>
			<div class="row">
				<div class="col-xl-9 offset-xl-2 mb-5 pb-5">
					<div class="sidepad">
						<p class="regwhite fade-upTxt">you’re unfortunate enough to be involved in an accident that wasn’t your fault, a claims management company will process a specific road traffic accident claim for the non-fault driver. This allows you to claim for damages, losses and injuries without affecting your own insurance policy.</p>
						<p class="gradient-pu fade-upTxt">As a claims management company, our team provide you with:</p>

						<ul class="custombulletsp regwhite">
							<li>24/7 Vehicle accident recovery</li>
							<li>Like-for-like vehicle replacement</li>
							<li>Full management of the claims process</li>
							<li>BSI Kitemark accredited accident repair</li>
							<li>Personal injury compensation claim</li>
						</ul>
					</div>
				
				</div>
			</div>
			
		</div>
	</div>
	
	<div class="non-fault-driver" style="background-image: url(assets/images/Starting-A-Non-Fault-Claim.jpg);">
		<div class="container">
			<div class="row">
				<div class="col-xl-6 offset-xl-6 pt-3">
					<div class="regpad">
						<p class="boldwhite"><span class="gradient-pu">As the non-fault driver</span>, it’s not fair that you have been involved in this scenario. It wasn’t you who DIDN’T follow the rules of the road, yet it IS YOU who has been hit with a range of inconveniences as a result.</p>
						<p class="regwhite">So, the accident market established Accident Claims Management (also known as Accident Management and Credit Hire).</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="black-bg better-alt blb">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="left-content">
						<div class="squarebcon1 ct-white-pur text-center">
							<h4 class="boldwhite">We are the <span class="gradient-pu">better alternative</span> than contacting your own insurer.</h4>
							<br>
							<p class="click-through boldwhite">Call our team now <a href="tel:+443309128010">0330 912 8010</a> | <a href="#"><span class="ct-under-pur">Start your claim now</span> <span class="ct-arrow-pur">&gt;</span></a></p>
						</div>
						<div class="squarebcon1 text-center">
							<p class="regwhite">From here, we take as much of the information that you currently have, open your accident claim and will also provide accident recovery if your vehicle is still stuck at the roadside.</p><p class="gradient-pu">
								<a href="/accident-management-company">Learn about Auto Claims Assist</a>
							</p>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="image">
						<img src="assets/images/Starting-A-Claim-Recovery.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="claim-block">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 mb-5">
				<p class="pheaders p5vh">By contacting <span class="gradient-45">Auto Claims Assist</span> first, you'll be entitled to a <span class="gradient-pu">much better</span> experience and at no cost to you:</p>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 col-md-4 col-xl-3 mb-3">
					<div class="item mb-3">
						<img class="mb-3" src="assets/images/Rated-Excellent-on-Trustpilot_3.svg" alt="">
						<p><b>Rated</b> EXCELLENT on Trustpilot</p>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 col-xl-3 mb-3">
					<div class="item mb-3">
						<img class="mb-3" src="assets/images/Award-Winning_2.svg" alt="">
						<p><b>Award-Winning</b> and FCA Regulated.</p>
					</div>
				</div>
				
				<div class="col-sm-6 col-md-4 col-xl-3 mb-3">
					<div class="item mb-3">
						<img class="mb-3" src="assets/images/No-Claims-Unaffected_2.svg" alt="">
						<p><b>Your</b> no claims bonus won't be affected</p>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 col-xl-3 mb-3">
					<div class="item mb-3">
						<img class="mb-3" src="assets/images/Like-For-Like-Replacement_2.svg" alt="">
						<p><b>Receive</b> a like-for-like vehicle.</p>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 col-xl-3 mb-3">
					<div class="item mb-3">
						<img class="mb-3" src="assets/images/Dedicated-Claims-Handler_1.svg" alt="">
						<p><b>Dedicated</b> claims handler.</p>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 col-xl-3 mb-3">
					<div class="item mb-3">
						<img class="mb-3" src="assets/images/Avoid-Paying-Your-Excess_1.svg" alt="">
						<p><b>Avoid</b> paying your insurance excess.</p>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 col-xl-3 mb-3">
					<div class="item mb-3">
						<img class="mb-3" src="assets/images/247-Recovery-and-Storage_2.svg" alt="">
						<p><b>24/7</b> Nationwide recovery &amp; storage.</p>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 col-xl-3 mb-3">
					<div class="item mb-3">
						<img class="mb-3" src="assets/images/Manufacturer-Approved-Parts_1.svg" alt="">
						<p><b>Manufacturer-approved</b> engineers and Access to a national repair network.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="image-top_btm-content">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="item-wrap">
						<div class="image mb-3">
							<img src="assets/images/Claims-Handler.jpg" alt="">
						</div>
						<div class="content iconpad grdar">
						<p class="regdark">To speak to an advisor for advice and guidance on getting started after a <a href="#">non-fault accident</a>, call <a href="tel:+443309128010">0330 912 8010</a>.</p>
						<p class="regdark">You can also <a href="#">start you non-fault claim online now</a>.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="item-wrap">
						<div class="image mb-3">
							<img src="assets/images/Non-Fault-Accident.jpg" alt="">
						</div>
						<div class="content iconpad grdar">
							<p class="regdark">Alternatively, you can fill out the form below and one of our New Claims Team will provide a call back to discuss the circumstances with you.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="request_form-block">
		<div class="container">
			<div class="form-wrap grdar ">
				<p><span style="font-size: 30px;">Request a <span class="gradient-45">callback</span>.</span><br> <span class="regdark">One of our advisors will call you back. Or, call us now on <a href="tel:+443309128010">0330 912 8010</a>.</span></p>
				<form>
					<div class="row">
						<div class="col-sm-6">
							<div class="mb-3">
								<label for="name" class="form-label">Name</label>
								<input type="text" class="form-control" id="name">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="mb-3">
								<label for="lastname" class="form-label">last Name</label>
								<input type="text" class="form-control" id="lastname">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="mb-3">
								<label for="email" class="form-label">Email address</label>
								<input type="email" class="form-control" id="email">
								</div>
						</div>
						<div class="col-sm-6">
							<div class="mb-3">
								<label for="phone" class="form-label">Phone</label>
								<input type="text" class="form-control" id="phone">
								</div>
						</div>
					</div>

					<button type="submit" class="btn btn-primary">Submit</button>
					</form>
			</div>
		</div>
	</div>

	<?php  include 'includes/footer.php';?>

